<?php
	/**
	 * Setting up acf related stuff here
	 */
	class ACFHandler {
		/**
		 * [_init entry point]
		 */
		public static function _init(){
			// register the fields
			add_action('acf/init', 'ACFHandler::register_fields');
		}
		/**
		 * [register_fields register the fields]
		 */
		public static function register_fields(){
			// register the module builder
			if( function_exists('acf_add_local_field_group') ){
				acf_add_local_field_group(array(
					'key' => 'group_default_template',
					'title' => 'Module Builder',
					'fields' => array(
						array(
							'key' => 'field_module_buidler',
							'label' => 'Module Builder',
							'name' => 'module_builder',
							'type' => 'flexible_content',
							'layouts' => Layout::get_layouts(),
							'button_label' => 'Add Module',
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'page_template',
								'operator' => '==',
								'value' => 'page-default.php'
							),
						),
					),
				));
			}
		}
	}

	ACFHandler::_init();
?>