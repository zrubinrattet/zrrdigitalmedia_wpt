<?php 

class SetupTheme{
	public static function _init(){
		show_admin_bar( false );
		add_action( 'after_setup_theme', 'SetupTheme::after_setup_theme' );
		add_action( 'wp_enqueue_scripts', 'SetupTheme::wp_enqueue_scripts' );
		add_action( 'init', 'SetupTheme::init' );
		// remove customizer from the Appearance menu
		add_action( 'admin_menu', 'SetupTheme::remove_menu_clutter', 9999 );
		// remove the customize button from the themes.php admin page
		add_action( 'admin_head-themes.php', 'SetupTheme::hide_customize_button' );
		// remove the comments from the admin menu bar
		add_action( 'wp_before_admin_bar_render', 'SetupTheme::remove_comments_from_admin_bar' );
		// remove the default widgets from the dashboard
		add_action( 'wp_dashboard_setup', 'SetupTheme::disable_default_dashboard_widgets', 999);
	}
	public static function init(){
		SetupTheme::clean_head();
		register_nav_menu( 'main-nav', 'Main Navigation' );

		// remove comment support
	    remove_post_type_support( 'post', 'comments' );
	    remove_post_type_support( 'page', 'comments' );
	}
	public static function after_setup_theme(){
		add_theme_support( 'html5' );
		add_theme_support( 'post-thumbnails' );	
	}
	public static function wp_enqueue_scripts(){
		SetupTheme::register_styles();
		SetupTheme::enqueue_styles();
		SetupTheme::register_javascript();
		SetupTheme::enqueue_javascript();	
	}
	/*
		Disable Default Dashboard Widgets
		@ https://digwp.com/2014/02/disable-default-dashboard-widgets/
	*/
	public static function disable_default_dashboard_widgets() {
		global $wp_meta_boxes;
		// wp..
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_site_health']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
		// bbpress
		unset($wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now']);
		// yoast seo
		unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);
		// gravity forms
		unset($wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard']);
	}
	public static function remove_menu_clutter(){
		global $submenu;
		// remove customizer
		if ( isset( $submenu[ 'themes.php' ] ) ) {
			foreach ( $submenu[ 'themes.php' ] as $index => $menu_item ) {
				if ( in_array( 'customize', $menu_item ) ) {
					unset( $submenu[ 'themes.php' ][ $index ] );
				}
			}
		}
		// remove comments
		remove_menu_page( 'edit-comments.php' );
	}
	public static function hide_customize_button(){
	    ?>
	    <style type="text/css">.theme-actions .button.hide-if-no-customize {display:none;} </style>
	    <?php
	}
	public static function remove_comments_from_admin_bar() {
	    global $wp_admin_bar;
	    $wp_admin_bar->remove_menu('comments');
	}
	private static function clean_head(){
		// removes generator tag
		remove_action( 'wp_head' , 'wp_generator' );
		// removes dns pre-fetch
		remove_action( 'wp_head', 'wp_resource_hints', 2 );
		// removes weblog client link
		remove_action( 'wp_head', 'rsd_link' );
		// removes windows live writer manifest link
		remove_action( 'wp_head', 'wlwmanifest_link');	
	}
	private static function enqueue_javascript(){
		wp_enqueue_script( 'theme' );
	}
	private static function enqueue_styles(){
		wp_enqueue_style( 'theme' );
	}

	private static function register_javascript(){
		wp_register_script( 'theme', get_template_directory_uri() . '/build/js/build.js' );
	}

	private static function register_styles(){
		wp_register_style( 'theme', get_template_directory_uri() . '/build/css/build.css' );
	}
}

SetupTheme::_init();

?>