<?php
$id = MiscUtil::get_field_from_loader_vars('wysiwyg_module_id', $post_id, $loader_vars);
$content = MiscUtil::get_field_from_loader_vars('wysiwyg_module_content', $post_id, $loader_vars, false);
?>
<section class="wysiwyg section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="wysiwyg-wrapper section-wrapper">
		<?php if( !empty($content) ): ?>
			<div class="wysiwyg-wrapper-content">
				<?php echo apply_filters( 'the_content', do_shortcode($content) ); ?>
			</div>
		<?php endif; ?>
	</div>
</section>