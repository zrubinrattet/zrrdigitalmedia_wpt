<?php 
	
	class WYSIWYG extends Layout{
		protected $layout = array(
			'key' => 'field_wysiwyg_module',
			'name' => 'wysiwyg_module',
			'label' => '<strong>WYSIWYG</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_wysiwyg_module_id',
					'label' => 'ID',
					'name' => 'wysiwyg_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_wysiwyg_module_content',
					'label' => 'Content',
					'name' => 'wysiwyg_module_content',
					'type' => 'wysiwyg',
				),
			)
		);
	}

?>