<?php

	class PartialUtil extends LoaderUtil{
		/**
		 * [$files the partials to register]
		 * @var array
		 */
		public static $files = array(
			'module.WYSIWYG' => '/src/partials/module.WYSIWYG.php',
		);
	}

?>