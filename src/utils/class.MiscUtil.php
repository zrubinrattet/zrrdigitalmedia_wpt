<?php

	class MiscUtil{
		/**
		 * [get_field_from_loader_vars used to get a field in a partial or component]
		 * @param  string      $field_name the field name
		 * @param  string|int  $id         the post's id
		 * @param  array       $lv         the loader_vars array
		 * @param  boolean     $formatting whether or not to 
		 * @return mixed                   the content from the field
		 */
		public static function get_field_from_loader_vars($field_name, $id, $lv, $formatting = true){
			// use the value in the loader vars if it exists
			if( isset($lv[$field_name]) && !empty($lv[$field_name]) ) return $lv[$field_name];
			// return nothing if it's not provided in loader vars and from_fc is set & is false
			if( isset($lv['from_fc']) && !$lv['from_fc'] && !isset($lv[$field_name]) ) return '';
			// otherwise get the value from the db
			return get_field($lv['mb_basename'] . '_' . $lv['fc_index'] . '_' . $field_name, $id, $formatting, $formatting);
		}
		/**
		 * [get_intelligent_id used mostly for internal purposes. this function will return an ID that's best passed in as a second param to get_field. if it's a post id it'll be either a string or an int of that post id. if it's a term it'll be term_ followed by the term_id like 'term_234']
		 * @return str|int the intelligent ID to be passed into get_field
		 */
		public static function get_intelligent_id(){
			// get the queried object
			$queried_object = get_queried_object();
			// setup wp_the_query
			global $wp_the_query;
			// sometimes when pages get redirected get_queried_object returns null
			if( is_null($queried_object) ){
				// use the post object in wp_the_query in this case
				$queried_object = $wp_the_query->post; 
			}
			// assign ID based on the queryed object class name
			switch (get_class($queried_object)) {
				case 'WP_Term':
					return 'term_' . strval($queried_object->term_id);
					break;
				
				case 'WP_Post':
					return $queried_object->ID;
					break;
			}
		}
	}
?>